FROM openjdk:8-alpine
COPY target/Api-Investimentos-0.0.1-SNAPSHOT.jar /home/ubuntu/deploys/Api-Investimentos-0.0.1-SNAPSHOT.jar
ARG mysql_host
ARG mysql_port
ARG mysql_user
ARG mysql_pass
ENV mysql_host=$mysql_host mysql_port=$mysql_port mysql_user=$mysql_user mysql_pass=$mysql_pass
WORKDIR /home/ubuntu/deploys
CMD ["java", "-jar", "-DskipTests", "Api-Investimentos-0.0.1-SNAPSHOT.jar"]